#ChatGPT End of the world or start of the world

Used for a while ChatGPT. Summary and impressions:

1. Can write a code
2. Interesting ideas about text generation.

## Generation of code

Try to create a sorting algorithms using Python / Lua / C / C++

Most of the time code is workable.
There are certain things remains outside of ChatGPT perception
Lua programming language not take into account that arrays are starting from 1 not from 0.
С/C++ does not follow best practices, can be possible memory leaks and stack corruptions


[Sorting in Wiki](https://en.wikipedia.org/wiki/Sorting_algorithm)

### Lua code & problems

Array in lua is starting from index 1 , this hack to make code workable force 0 index initialize.

```
arr = { [0]
```

Example of heapsort code is not workable,fix needed.

```
function heapify(arr, n, i)
    local largest = i
    local l = 2 * i + 1
    local r = 2 * i + 2

    if l < n and arr[l] > arr[largest] then
        largest = l
    end

    if r < n and arr[r] > arr[largest] then
        largest = r
    end

    if largest ~= i then
        arr[i], arr[largest] = arr[largest], arr[i]
        heapify(arr, n, largest)
    end
end

function heapSort(arr)
    local n = #arr
    for i = math.floor(n/2), 1, -1 do
        heapify(arr, n, i-1)
    end

    for i = n, 2, -1 do
        arr[i], arr[1] = arr[1], arr[i]
        heapify(arr, i-1, 0)
    end
end
-- ChatGPT 0!!!!!
arr = { [0]= 3, 4, 1, 7, 2, 9, 8, 6}

heapSort(arr)

print("Sorted array is:")
for i = 0, #arr do
    print(arr[i])
end
```

So in nutshell code workable , but not perfect.

### C/c++ Examples

Check the input values, or memory boundaries it's not for us:)What about NULL approaching:)?

```
void swap(int *a, int *b) {
    int temp = *a;
    *a = *b;
    *b = temp;
}
```

Full code example

```
#include <stdio.h>

void swap(int *a, int *b) {
    int temp = *a;
    *a = *b;
    *b = temp;
}

int partition(int arr[], int low, int high) {
    int pivot = arr[high];
    int i = (low - 1);
    for (int j = low; j <= high - 1; j++) {
        if (arr[j] < pivot) {
            i++;
            swap(&arr[i], &arr[j]);
        }
    }
    swap(&arr[i + 1], &arr[high]);
    return (i + 1);
}

void quicksort(int arr[], int low, int high) {
    if (low < high) {
        int pi = partition(arr, low, high);
        quicksort(arr, low, pi - 1);
        quicksort(arr, pi + 1, high);
    }
}

int main() {
    int arr[] = {10, 7, 8, 9, 1, 5};
    int n = sizeof(arr) / sizeof(arr[0]);
    quicksort(arr, 0, n - 1);
    for (int i = 0; i < n; i++)
        printf("%d ", arr[i]);
    return 0;
}

```
To make an analasys is really unpleasant task, it's a bit clunky for now.

Twin brother C++ version of same code.
```
#include <algorithm>
#include <iostream>
#include <vector>

using namespace std;

int partition(vector<int> &arr, int low, int high) {
    int pivot = arr[high];
    int i = (low - 1);
    for (int j = low; j <= high - 1; j++) {
        if (arr[j] < pivot) {
            i++;
            swap(arr[i], arr[j]);
        }
    }
    swap(arr[i + 1], arr[high]);
    return (i + 1);
}

void quicksort(vector<int> &arr, int low, int high) {
    if (low < high) {
        int pi = partition(arr, low, high);
        quicksort(arr, low, pi - 1);
        quicksort(arr, pi + 1, high);
    }
}

int main() {
    vector<int> arr = {10, 7, 8, 9, 1, 5};
    int n = arr.size();
    quicksort(arr, 0, n - 1);
    for (int i = 0; i < n; i++)
        cout << arr[i] << " ";
    return 0;
}
```

Code workable but with some design issues.

### Python 

Most of the time code is workable.
Here is an example of the Bubble sort.

```
def bubble_sort(arr):
    n = len(arr)
    for i in range(n):
        for j in range(0, n-i-1):
            if arr[j] > arr[j+1] :
                arr[j], arr[j+1] = arr[j+1], arr[j]
    return arr

# Test the function with an example array
arr = [64, 34, 25, 12, 22, 11, 90]
print("Sorted array is:", bubble_sort(arr))
```

Sometimes code does not fly.

```
# Non workable code.
def cubesort(arr):
    def recursion(start, end):
        if end - start <= 1:
            return
        pivot = (end - start + 1) // 3
        recursion(start, end - pivot)
        recursion(start + pivot, end)
        recursion(start, end - pivot)

    recursion(0, len(arr) - 1)
    return arr

arr = [84,64, 25, 12, 22, 11]
print(cubesort(arr))
```

### Thoughts, What is next?

1. It will be better in future.
2. Internet will have this copy pasted code, security/quality/e.t.c. will be highly impacted.
3. Does not understand, memory , arrays and specifics of the certain programming languages
4. Later will make a review again.

Version 1, 03 February 2023.

[Blog Repository](https://gitlab.com/bazilxp/blog)

[Main](/)