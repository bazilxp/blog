#ChatGPT Конец света или начало света

Попробовал ChatGPT, первые впечатления:

1. Умеет писать код
2. Интересные идеи по генерации текста, спрашивал разные вопросы 

## Генерация кода

Попробовал создавать алгоритмы сортировки на Python / Lua / C / C++

Код получается рабочий качество кода можно оценить.
Есть тонкости,которые остаются вне восприятия
Язык программирования LUA массивы начинаютс с индекса 1 , а не 0. Код не учитывает данные особенности
С/C++ не соблюдаются лучшие практики.


[Сортировка в википедии](https://en.wikipedia.org/wiki/Sorting_algorithm)

### Lua код и проблемки

Проблема с данным кода выражется довольно просто в Lua массивы начинаются с 1 а не 0 , кусок кода форсирует инициализацию массива с 0.
```
arr = { [0]
```
Сортировка Кучами(HeapSort).

```
function heapify(arr, n, i)
    local largest = i
    local l = 2 * i + 1
    local r = 2 * i + 2

    if l < n and arr[l] > arr[largest] then
        largest = l
    end

    if r < n and arr[r] > arr[largest] then
        largest = r
    end

    if largest ~= i then
        arr[i], arr[largest] = arr[largest], arr[i]
        heapify(arr, n, largest)
    end
end

function heapSort(arr)
    local n = #arr
    for i = math.floor(n/2), 1, -1 do
        heapify(arr, n, i-1)
    end

    for i = n, 2, -1 do
        arr[i], arr[1] = arr[1], arr[i]
        heapify(arr, i-1, 0)
    end
end
-- ChatGPT 0!!!!!
arr = { [0]= 3, 4, 1, 7, 2, 9, 8, 6}

heapSort(arr)

print("Sorted array is:")
for i = 0, #arr do
    print(arr[i])
end
```


### C/c++ Примеры

Память или проверки значений для слабаков
тот же null и так даллее...

```
void swap(int *a, int *b) {
    int temp = *a;
    *a = *b;
    *b = temp;
}
```

Полный код для примера

```
#include <stdio.h>

void swap(int *a, int *b) {
    int temp = *a;
    *a = *b;
    *b = temp;
}

int partition(int arr[], int low, int high) {
    int pivot = arr[high];
    int i = (low - 1);
    for (int j = low; j <= high - 1; j++) {
        if (arr[j] < pivot) {
            i++;
            swap(&arr[i], &arr[j]);
        }
    }
    swap(&arr[i + 1], &arr[high]);
    return (i + 1);
}

void quicksort(int arr[], int low, int high) {
    if (low < high) {
        int pi = partition(arr, low, high);
        quicksort(arr, low, pi - 1);
        quicksort(arr, pi + 1, high);
    }
}

int main() {
    int arr[] = {10, 7, 8, 9, 1, 5};
    int n = sizeof(arr) / sizeof(arr[0]);
    quicksort(arr, 0, n - 1);
    for (int i = 0; i < n; i++)
        printf("%d ", arr[i]);
    return 0;
}

```
Анализировать косяки сильно не хочется ,но понятно что их тут больше. Некоторый код не проходит критику компилятора но это уже отдельная история....

Версия на С++ брат близнец
```
#include <algorithm>
#include <iostream>
#include <vector>

using namespace std;

int partition(vector<int> &arr, int low, int high) {
    int pivot = arr[high];
    int i = (low - 1);
    for (int j = low; j <= high - 1; j++) {
        if (arr[j] < pivot) {
            i++;
            swap(arr[i], arr[j]);
        }
    }
    swap(arr[i + 1], arr[high]);
    return (i + 1);
}

void quicksort(vector<int> &arr, int low, int high) {
    if (low < high) {
        int pi = partition(arr, low, high);
        quicksort(arr, low, pi - 1);
        quicksort(arr, pi + 1, high);
    }
}

int main() {
    vector<int> arr = {10, 7, 8, 9, 1, 5};
    int n = arr.size();
    quicksort(arr, 0, n - 1);
    for (int i = 0; i < n; i++)
        cout << arr[i] << " ";
    return 0;
}
```

### Python 

Код в большинстве случаев рабочий. Явных проблем не налюдается

Метод пузырька

```
def bubble_sort(arr):
    n = len(arr)
    for i in range(n):
        for j in range(0, n-i-1):
            if arr[j] > arr[j+1] :
                arr[j], arr[j+1] = arr[j+1], arr[j]
    return arr

# Test the function with an example array
arr = [64, 34, 25, 12, 22, 11, 90]
print("Sorted array is:", bubble_sort(arr))
```

Некоторые метода не летают :)

```
# не рабочий код
def cubesort(arr):
    def recursion(start, end):
        if end - start <= 1:
            return
        pivot = (end - start + 1) // 3
        recursion(start, end - pivot)
        recursion(start + pivot, end)
        recursion(start, end - pivot)

    recursion(0, len(arr) - 1)
    return arr

arr = [84,64, 25, 12, 22, 11]
print(cubesort(arr))
```

### Мысли на тему что дальше

1. Станет лучше со временем
2. Интернет наполнится скопированым кодом,код будет привносить некоторый колорит. Возможно количество не безопасного кода в какой то момент будет зашкаливать.
3. Не понимает специфику языка, индексы ,граничные значения, утечки памяти и так далее...
4. Вернусь к этой теме потом еще раз.

Версия 1, 03 Февраля 2023.

[Репозиторий блога](https://gitlab.com/bazilxp/blog)

[На главную](/)