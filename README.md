# Open source blog for using markdown and render as html

Small history, a little bit tired to maintain WordPress:
Main concerns:

1. Security (plugins anti spam, install new versions)
2. No time , no energy to maintain this tool
3. Complicated setup in terms hosting should have Php & MySql and apache or nginx
4. Accounts Do I really need them ?

What finally got in fact:
I can bring as many as i need markdown files. Using any IDE, also GitLab UI
I have cheap virtual machine where is shell runner deployed due to memory constraints.

Project tree is straight forward:
* README.md
* .gitlab-ci.yml

You adding more *md files*

## requirements

1. gitlab runner -  Linux (unix) , `shell type`
2. python 3.x
3. markdown python library 
4. xargs :)

### For Runner registration refer to gitlab document
https://docs.gitlab.com/runner/register/

### Python setup for runner
On the gitlab runner machine , prepare own python virtual environment
```
sudo -u gitlab-runner -i
python3 -m venv ~/venv-markdown
source ~/venv-markdown/bin/activate
pip install markdown
```

### Example of '.gitlab-ci.yml' 
```
pages:
  tags: 
    - blog-runner
  script:
    - source ~/venv-markdown/bin/activate
    - mkdir -p public/
    - ls *.md  |  xargs -I{} sh -c "python -m markdown -x tables -x fenced_code {} > public/{}.html"
    - cd public/ ; ls *.md.html  | xargs -I{} sh -c "echo '<a href={}>/{}</a></br>' >> index.html"
    
  artifacts:
    paths:
      - public
```

please refer to [Python markdown extensions](https://python-markdown.github.io/extensions/)
In brief extensions are enabled by "-x"
```
python -m markdown -x tables -x fenced_code README.md > output.html
```

[Blog source code](https://gitlab.com/bazilxp/blog)

[Back to Index](/)