# Sorting tool

Version 1.0alpha

Written using C language the [Raylib.com](https://raylib.com/) . Visual tool 2023 (c) Vasiliy Evstigneev


# Usage help

Keyboard Left arrow - previous method
Keyboard Right arrow - next method

Mouse left  click over Triangles doing the similar as Laft and Right

[Try out sorting tool](extra/sorting.html)


Supported sorting methods , written by ChatGPT

| N |Method     | Description |
| -----------| ----------- | ----------- |
|0|Bubble sort|[Code bubblesort_ai.c](https://gitlab.com/bazilxp/blog/-/blob/01-ChatGPT-Sorting/chat-gpt-c/bubblesort_ai.c)|
|1|Block sort |[Code blocksort_ai.c](https://gitlab.com/bazilxp/blog/-/blob/01-ChatGPT-Sorting/chat-gpt-c/blocksort_ai.c)           |
|2|Cocktail sort| [Code cocktailsort_ai.c](https://gitlab.com/bazilxp/blog/-/blob/01-ChatGPT-Sorting/chat-gpt-c/cocktailsort_ai.c) |
|3|Comb sort    | [Code combsort_ai.c](https://gitlab.com/bazilxp/blog/-/blob/01-ChatGPT-Sorting/chat-gpt-c/combsort_ai.c)|
|4|Cube sort| [Code cubesort_ai.c](https://gitlab.com/bazilxp/blog/-/blob/01-ChatGPT-Sorting/chat-gpt-c/cubesort_ai.c)|
|5|Cycle sort | [Code cyclesort_ai.c](https://gitlab.com/bazilxp/blog/-/blob/01-ChatGPT-Sorting/chat-gpt-c/cyclesort_ai.c)|
|6|Exchange sort| [Code exchangesort_ai.c](https://gitlab.com/bazilxp/blog/-/blob/01-ChatGPT-Sorting/chat-gpt-c/exchangesort_ai.c) |
|7|Gnome sort| [Code exchangesort_ai.c](https://gitlab.com/bazilxp/blog/-/blob/01-ChatGPT-Sorting/chat-gpt-c/gnomesort_ai.c) |
|8|Heap sort| [Code heapsort_ai.c](https://gitlab.com/bazilxp/blog/-/blob/01-ChatGPT-Sorting/chat-gpt-c/heapsort_ai.c) |
|9|Insertion sort| [Code insertionsort_ai.c](https://gitlab.com/bazilxp/blog/-/blob/01-ChatGPT-Sorting/chat-gpt-c/insertionsort_ai.c) |
|10|Intro sort| [Code introsort_ai.c](https://gitlab.com/bazilxp/blog/-/blob/01-ChatGPT-Sorting/chat-gpt-c/introsort_ai.c) |
|11|Library sort| [Code librarysort_ai.c](https://gitlab.com/bazilxp/blog/-/blob/01-ChatGPT-Sorting/chat-gpt-c/librarysort_ai.c) |
|12|Merge sort| [Code mergesort_ai.c](https://gitlab.com/bazilxp/blog/-/blob/01-ChatGPT-Sorting/chat-gpt-c/mergesort_ai.c) |
|13|Oddeven sort| [OddEven oddevensort_ai.c](https://gitlab.com/bazilxp/blog/-/blob/01-ChatGPT-Sorting/chat-gpt-c/oddevnsort_ai.c) |
|14|Quick sort| [Code quicksort_ai.c](https://gitlab.com/bazilxp/blog/-/blob/01-ChatGPT-Sorting/chat-gpt-c/quicksort_ai.c) |
|15|Selection sort| [Code selectionsort_ai.c](https://gitlab.com/bazilxp/blog/-/blob/01-ChatGPT-Sorting/chat-gpt-c/selectionsort_ai.c) |
|16|Shell sort| [Code shellsort_ai.c](https://gitlab.com/bazilxp/blog/-/blob/01-ChatGPT-Sorting/chat-gpt-c/shellsort_ai.c) |
|17|Smooth sort| [Code smoothsort_ai.c](https://gitlab.com/bazilxp/blog/-/blob/01-ChatGPT-Sorting/chat-gpt-c/smoothsort_ai.c) |
|18|Strand sort| [Code strandsort_ai.c](https://gitlab.com/bazilxp/blog/-/blob/01-ChatGPT-Sorting/chat-gpt-c/strandsort_ai.c) |
|19|Tim sort| [Code timsort_ai.c](https://gitlab.com/bazilxp/blog/-/blob/01-ChatGPT-Sorting/chat-gpt-c/timsort_ai.c) |




[All source code of sorting](https://gitlab.com/bazilxp/blog/-/tree/01-ChatGPT-Sorting/chat-gpt-c)

[Blog Repository](https://gitlab.com/bazilxp/blog)

[Main](/)
