# Блог с открытым кодом

Немножко предистории о Wordpress устал:

1. Безопасность  ( нужно обновлять, плагины для анти спама и так далее)
2. Нет времени , нет энергии 
3. Сложно поддерживать hosting должен иметь PHP,MySQL,Apache/nginx
4. Учетные записи ?нужни ли они?

Долго думал решил просто писать файлы на языке разметки МД.

Дерево проекта:

* README.md
* .gitlab-ci.yml

Добавляем больше файлов

## Требования 

1. gitlab runner -  Linux (unix) , `shell type`
2. python 3.x
3. markdown python library 
4. xargs

### Регистрация и установка gitlab-runner 
https://docs.gitlab.com/runner/register/

### Настройка питона для gitlab-runner
Установка виртуальной среды для питона на машине gitlab-runner
```
sudo -u gitlab-runner -i
python3 -m venv ~/venv-markdown
source ~/venv-markdown/bin/activate
pip install markdown
```

### Пример of '.gitlab-ci.yml' 
```
pages:
  tags: 
    - blog-runner
  script:
    - source ~/venv-markdown/bin/activate
    - mkdir -p public/
    - ls *.md  |  xargs -I{} sh -c "python -m markdown -x tables -x fenced_code {} > public/{}.html"
    - cd public/ ; ls *.md.html  | xargs -I{} sh -c "echo '<a href={}>/{}</a></br>' >> index.html"
    
  artifacts:
    paths:
      - public
```

Смотрите документацию [Python markdown extensions](https://python-markdown.github.io/extensions/)
Расширения для питона подлючаются через "-x" к примеру таблицы и код
```
python -m markdown -x tables -x fenced_code README.md > output.html
```

[Репозиторий блога](https://gitlab.com/bazilxp/blog)

[Back to Index](/)